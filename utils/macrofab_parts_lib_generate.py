import macrofab_parts_list

FMT_DOC_LIB = \
r"""EESchema-DOCLIB  Version 2.0
%s
#
#End Doc Library"""

FMT_DOC_LIB_ITEM = \
r"""#
$CMP %s
D %s
$ENDCMP
"""

FMT_EESCHEMA_LIB = \
r"""EESchema-LIBRARY Version 2.3
#encoding utf-8
%s
#
#End Library
"""

FMT_COMP_CAPACITOR_NP = \
r"""
#
# %s
#
DEF %s C 0 10 N Y 1 F N
F0 "C" 50 10 50 H V L CNN
F1 "%s" 50 -60 50 H V L CNN
F2 "" 0 0 60 H V C CNN
F3 "" 0 0 60 H V C CNN
F4 "%s" 50 -130 50 H I L CNN "MPN"
$FPLIST
*%s
$ENDFPLIST
DRAW
P 3 0 1 0  -50 0  50 0  50 0 N
P 3 0 1 0  50 -50  -50 -50  -50 -50 N
X ~ 1 0 50 50 D 40 40 1 1 P
X ~ 2 0 -100 50 U 40 40 1 1 P
ENDDRAW
ENDDEF
"""

FMT_COMP_CAPACITOR_POL = \
r"""#
# %s
#
DEF %s C 0 10 N N 1 F N
F0 "C" 25 100 50 H V L CNN
F1 "%s" 25 -100 50 H V L CNN
F2 "" 0 0 60 H V C CNN
F3 "" 0 0 60 H V C CNN
F4 "%s" 25 -170 50 H I L CNN "MPN"
$FPLIST
*%s
$ENDFPLIST
DRAW
A 0 -150 128 1287 513 0 1 20 N -80 -50 80 -50
P 2 0 1 20  -80 30  80 30 N
P 2 0 1 0  -70 90  -30 90 N
P 2 0 1 0  -50 70  -50 110 N
X ~ 1 0 150 110 D 40 40 1 1 P
X ~ 2 0 -150 130 U 40 40 1 1 P
ENDDRAW
ENDDEF
"""


FMT_COMP_RESISTOR = \
r"""
#
# %s
#
DEF %s R 0 0 N Y 1 F N
F0 "R" -50 50 50 V V C CNN
F1 "%s" 50 50 50 V V C CNN
F2 "" 0 0 60 H V C CNN
F3 "" 0 0 60 H V C CNN
F4 "%s" 120 50 50 V I C CNN "MPN"
$FPLIST
*%s
$ENDFPLIST
DRAW
S 20 50 -20 -50 0 1 0 N
X ~ 1 0 100 50 D 60 60 1 1 P
X ~ 2 0 -100 50 U 60 60 1 1 P
ENDDRAW
ENDDEF
"""


FMT_COMP_INDUCTOR = \
r"""
#
# %s
#
DEF %s L 0 40 N N 1 F N
F0 "L" -50 0 40 V V C CNN
F1 "%s" 100 0 40 V V C CNN
F2 "" 0 0 60 H V C CNN
F3 "" 0 0 60 H V C CNN
F4 "%s" 170 0 40 V I C CNN "MPN"
$FPLIST
*%s
$ENDFPLIST
DRAW
A 0 -75 25 -899 899 0 1 0 N 0 -100 0 -50
A 0 -25 25 -899 899 0 1 0 N 0 -50 0 0
A 0 25 25 -899 899 0 1 0 N 0 0 0 50
A 0 75 25 -899 899 0 1 0 N 0 50 0 100
X 1 1 0 150 50 D 70 70 1 1 P
X 2 2 0 -150 50 U 70 70 1 1 P
ENDDRAW
ENDDEF
"""


def main():
    result_fp = set()
    result_libitems = []
    result_docitems = []
    for [pn,pd,pc] in macrofab_parts_list.passives:
        [p_t, p_type, p_fp, p_v] = pn.split("-")
        if ("-CAP-" in pn):
            if ("Electrolytic" not in pd):
                p_fp = "c_"+p_fp.lower()
                libitem_fmt = FMT_COMP_CAPACITOR_NP
            else:
                p_fp = macrofab_parts_list.c_electrolytes[pn]
                if not p_fp:
                    continue
                p_fp = "c_elec_"+p_fp.lower()
                libitem_fmt = FMT_COMP_CAPACITOR_POL
        elif ("-RES-" in pn):
            if ("Array" not in pd) and ("Trim" not in pd):
                p_fp = "r_"+p_fp.lower()
                libitem_fmt = FMT_COMP_RESISTOR
            else:
                continue
        elif ("-IND-" in pn):
            p_fp = "l_"+p_fp.lower()
            libitem_fmt = FMT_COMP_INDUCTOR
        else:
            p_fp = None

        if p_fp:
            result_fp.add(p_fp)
            libitem = libitem_fmt % (pn,pn,pn,pn,p_fp)
            result_libitems.append(libitem)
            docitem = FMT_DOC_LIB_ITEM % (pn, pd + " (%s)"%pc)
            result_docitems.append(docitem)

    with open("../macrofab_passives.dcm", "w") as ofile:
        ofile.write(FMT_DOC_LIB % ("".join(result_docitems).strip()))
    with open("../macrofab_passives.lib", "w") as ofile:
        ofile.write(FMT_EESCHEMA_LIB % "".join(result_libitems))
    print sorted(list(result_fp))

if __name__ == '__main__':
    main()